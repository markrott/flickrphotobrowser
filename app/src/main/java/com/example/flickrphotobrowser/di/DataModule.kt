package com.example.flickrphotobrowser.di

import com.example.flickrphotobrowser.data.contract.FlickrRepositoryContract
import com.example.flickrphotobrowser.data.impl.FlickrRepositoryImpl
import com.example.flickrphotobrowser.data.impl.GlideLoadImage
import com.example.flickrphotobrowser.data.contract.LoadImageContract
import com.example.flickrphotobrowser.ui.PhotoBrowserViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dataModule = module {
    single<LoadImageContract> { GlideLoadImage() }
    single<FlickrRepositoryContract> { FlickrRepositoryImpl(get()) }
    viewModel { PhotoBrowserViewModel(get()) }
}