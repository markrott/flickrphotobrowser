package com.example.flickrphotobrowser.di

import com.example.flickrphotobrowser.BuildConfig
import com.example.flickrphotobrowser.api.FlickrApi
import com.example.flickrphotobrowser.api.ParamsInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { ParamsInterceptor() }
    single { provideOkHttpClient(get()) }
    single { provideRetrofit(get()) }
    single { provideAppApi(get()) }
}

private fun provideOkHttpClient(paramsInterceptor: ParamsInterceptor): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    logging.level = (HttpLoggingInterceptor.Level.NONE)
    return OkHttpClient()
        .newBuilder()
        .addInterceptor(paramsInterceptor)
        .addInterceptor(logging)
        .build()
}

private fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit
        .Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

private fun provideAppApi(retrofit: Retrofit): FlickrApi = retrofit.create(FlickrApi::class.java)