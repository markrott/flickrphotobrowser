package com.example.flickrphotobrowser.api

import com.example.flickrphotobrowser.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class ParamsInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var req = chain.request()
        val newUrl = req
                .url
                .newBuilder()
                .addQueryParameter("api_key", BuildConfig.API_KEY)

                /**
                 * I decided to add these parameters to the request directly
                 * in the API method: `fetchRecentPhotos()`
                 */
//            .addQueryParameter("method", "flickr.photos.getrecent")
//            .addQueryParameter("format", "json")
//            .addQueryParameter("nojsoncallback", "1")
//            .addQueryParameter("extras", "url_z")

                .build()

        req = req.newBuilder().url(newUrl).build()
        return chain.proceed(req)
    }
}