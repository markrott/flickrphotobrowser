package com.example.flickrphotobrowser.api

import com.example.flickrphotobrowser.utils.PER_PAGE
import com.example.flickrphotobrowser.entity.remote.RecentImagesDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface FlickrApi {

    @GET("rest/")
    suspend fun fetchRecentPhotos(
        @Query("page") page: Int,
        @Query("per_page") perPage: Int = PER_PAGE,

        /**
         * You can set these parameters in the class: ParamsInterceptor.kt
         */
        @Query("method") method: String = "flickr.photos.getrecent",
        @Query("format") format: String = "json",
        @Query("nojsoncallback") noJsonCallback: String = "1",
        @Query("extras") extras: String = "url_z"
    ) : Response<RecentImagesDto>
}