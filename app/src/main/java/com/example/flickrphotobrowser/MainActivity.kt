package com.example.flickrphotobrowser

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.flickrphotobrowser.ui.PhotoBrowserFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_main)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.frm_container, PhotoBrowserFragment.newInstance())
                .commit()
        }
    }
}