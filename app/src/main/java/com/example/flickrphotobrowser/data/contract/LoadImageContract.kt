package com.example.flickrphotobrowser.data.contract

import android.widget.ImageView

interface LoadImageContract {
    fun loadImage(url: String, targetView: ImageView)
}