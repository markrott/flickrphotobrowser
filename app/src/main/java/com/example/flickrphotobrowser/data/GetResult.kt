package com.example.flickrphotobrowser.data

import android.util.Log
import com.example.flickrphotobrowser.utils.APP_TAG
import retrofit2.Response
import com.example.flickrphotobrowser.api.Result

suspend fun <T> getResult(action: suspend () -> Response<T>): Result<T> {
    try {
        val response = action()
        if (response.isSuccessful) {
            val body = response.body()
            if (body != null) return Result.success(body)
        }

        val finalMsg = error("${response.code()} ${response.message()}")
        return Result.error(Exception(finalMsg))

    } catch (e: Exception) {
        Log.e(APP_TAG, "Get result exception: ${e.message}")
        return Result.error(e)
    }
}

private fun error(msg: String): String {
    val finalMsg = "Network call has failed for a following reason: $msg"
    Log.e(APP_TAG, finalMsg)
    return finalMsg
}