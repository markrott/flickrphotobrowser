package com.example.flickrphotobrowser.data.impl

import android.util.Log
import com.example.flickrphotobrowser.api.FlickrApi
import com.example.flickrphotobrowser.api.Status
import com.example.flickrphotobrowser.data.contract.FlickrRepositoryContract
import com.example.flickrphotobrowser.data.getResult
import com.example.flickrphotobrowser.entity.toUi
import com.example.flickrphotobrowser.entity.ui.PhotosUi
import com.example.flickrphotobrowser.utils.APP_TAG
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class FlickrRepositoryImpl(private val api: FlickrApi) : FlickrRepositoryContract {

    override suspend fun fetchRecentPhotos(page: Int): PhotosUi {
        return withContext(Dispatchers.IO) {
            Log.d(APP_TAG, "Thread: ${Thread.currentThread().name}")
            val result = getResult { api.fetchRecentPhotos(page) }
            if(result.status == Status.ERROR) throw result.exception ?: IllegalArgumentException("Stub exception")
            return@withContext result.data?.photos?.toUi() ?: PhotosUi()
        }
    }
}