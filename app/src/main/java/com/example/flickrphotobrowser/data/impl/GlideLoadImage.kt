package com.example.flickrphotobrowser.data.impl

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.flickrphotobrowser.R
import com.example.flickrphotobrowser.data.contract.LoadImageContract

class GlideLoadImage : LoadImageContract {

    override fun loadImage(url: String, targetView: ImageView) {
        Glide
                .with(targetView.context)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_background)
                .into(targetView)
    }
}