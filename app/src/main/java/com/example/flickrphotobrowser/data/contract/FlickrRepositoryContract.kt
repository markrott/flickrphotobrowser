package com.example.flickrphotobrowser.data.contract

import com.example.flickrphotobrowser.entity.ui.PhotosUi

interface FlickrRepositoryContract {

    suspend fun fetchRecentPhotos(page: Int) : PhotosUi
}