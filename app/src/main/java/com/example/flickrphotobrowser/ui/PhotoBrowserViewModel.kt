package com.example.flickrphotobrowser.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.example.flickrphotobrowser.utils.PER_PAGE

import com.example.flickrphotobrowser.data.contract.FlickrRepositoryContract
import com.example.flickrphotobrowser.entity.ui.PhotoUi
import com.example.flickrphotobrowser.ui.adapters.paging.PhotoDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class PhotoBrowserViewModel(private val repository: FlickrRepositoryContract) : ViewModel() {

    private val uniqueIdsSet = mutableSetOf<Long>()

    val pagingFlow = getPagerFlow()
            .map { data -> filterSameImagesById(data) }

    private fun getPagerFlow(): Flow<PagingData<PhotoUi>> =
            Pager(PagingConfig(PER_PAGE)) { PhotoDataSource(repository) }
                    .flow
                    .cachedIn(viewModelScope)

    /**
     * I added this logic to avoid showing identical images that come from the Flickr service.
     * Filtering is performed using photoID
     */
    private fun filterSameImagesById(data: PagingData<PhotoUi>): PagingData<PhotoUi> =
            data
                    .filter { it.id.toLong() !in uniqueIdsSet }
                    .map {
                        uniqueIdsSet.add(it.id.toLong())
                        it
                    }
}