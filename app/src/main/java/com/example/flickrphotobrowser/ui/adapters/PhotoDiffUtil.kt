package com.example.flickrphotobrowser.ui.adapters

import androidx.recyclerview.widget.DiffUtil
import com.example.flickrphotobrowser.entity.ui.PhotoUi

class PhotoDiffUtil : DiffUtil.ItemCallback<PhotoUi>() {

    override fun areItemsTheSame(oldItem: PhotoUi, newItem: PhotoUi): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: PhotoUi, newItem: PhotoUi): Boolean =
        oldItem == newItem
}