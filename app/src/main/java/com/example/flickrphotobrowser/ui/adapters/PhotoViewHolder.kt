package com.example.flickrphotobrowser.ui.adapters

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.flickrphotobrowser.R
import com.example.flickrphotobrowser.data.contract.LoadImageContract
import com.example.flickrphotobrowser.entity.ui.PhotoUi
import org.koin.core.KoinComponent
import org.koin.core.inject

class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), KoinComponent {

    private val imageLoader: LoadImageContract by inject()
    private val ivPhoto: AppCompatImageView = itemView.findViewById(R.id.iv_photo)
    private val tvTitle: AppCompatTextView = itemView.findViewById(R.id.tv_title)

    fun bind(model: PhotoUi) {
        tvTitle.text = model.title
        if (model.url.isNotEmpty()) {
            imageLoader.loadImage(model.url, ivPhoto)
        } else {
            ivPhoto.setImageResource(R.drawable.ic_launcher_background)
        }
    }
}