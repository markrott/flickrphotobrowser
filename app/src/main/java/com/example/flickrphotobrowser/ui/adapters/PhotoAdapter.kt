package com.example.flickrphotobrowser.ui.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import com.example.flickrphotobrowser.R
import com.example.flickrphotobrowser.entity.ui.PhotoUi
import com.example.flickrphotobrowser.utils.inflate

class PhotoAdapter : PagingDataAdapter<PhotoUi, PhotoViewHolder>(PhotoDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder =
        PhotoViewHolder(parent.inflate(R.layout.item_photo_and_label))

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val model = getItem(position)
        model?.let { holder.bind(it) }
    }
}