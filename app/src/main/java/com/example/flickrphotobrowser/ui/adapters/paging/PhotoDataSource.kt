package com.example.flickrphotobrowser.ui.adapters.paging

import android.util.Log
import androidx.paging.PagingSource
import com.example.flickrphotobrowser.utils.APP_TAG
import com.example.flickrphotobrowser.data.contract.FlickrRepositoryContract
import com.example.flickrphotobrowser.entity.ui.PhotoUi

class PhotoDataSource(private val repo: FlickrRepositoryContract) : PagingSource<Int, PhotoUi>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PhotoUi> {
        return try {

            val page = params.key ?: 1
            Log.d(APP_TAG, "Start load new data. Page = $page")
            val response = repo.fetchRecentPhotos(page)

            LoadResult.Page(
                data = response.photo,
                prevKey = null,
                nextKey = response.page + 1
            )

        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}