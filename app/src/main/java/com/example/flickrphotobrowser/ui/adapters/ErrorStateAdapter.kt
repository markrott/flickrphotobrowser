package com.example.flickrphotobrowser.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.flickrphotobrowser.R
import com.example.flickrphotobrowser.utils.inflate

class ErrorStateAdapter(
        private val retry: () -> Unit
) : LoadStateAdapter<ErrorStateAdapter.LoadStateViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder =
            LoadStateViewHolder(parent.inflate(R.layout.item_load_more), retry)

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    class LoadStateViewHolder(
            itemView: View,
            private val retry: () -> Unit
    ) : RecyclerView.ViewHolder(itemView) {

        private val btnRetry = itemView.findViewById<AppCompatButton>(R.id.btn_retry)
        private val txtErrorMessage = itemView.findViewById<AppCompatTextView>(R.id.tv_load_more_label)

        fun bind(loadState: LoadState) {
            if (loadState is LoadState.Error) {
                txtErrorMessage.text = loadState.error.localizedMessage
            }
            btnRetry.setOnClickListener { retry.invoke() }
        }
    }
}