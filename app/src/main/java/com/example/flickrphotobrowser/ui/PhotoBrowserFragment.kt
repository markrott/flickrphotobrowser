package com.example.flickrphotobrowser.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.flickrphotobrowser.R
import com.example.flickrphotobrowser.ui.adapters.ErrorStateAdapter
import com.example.flickrphotobrowser.ui.adapters.PhotoAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class PhotoBrowserFragment : Fragment() {

    private val photoAdapter: PhotoAdapter = PhotoAdapter()
    private val viewModel: PhotoBrowserViewModel by viewModel()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.frg_photo_browser, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView(view)
        subscribeToReceiveData()
    }

    private fun initRecyclerView(view: View) {
        /**
         * I use this option because: kotlinx.android.synthetic is no longer a recommended practice.
         * Removing in favour of explicit findViewById
         * Links:
         * https://youtrack.jetbrains.com/issue/KT-42121
         * https://habr.com/ru/post/526192/
         */
        val rcvPhoto: RecyclerView = view.findViewById(R.id.rcv_photo)

        //bind the LoadStateAdapter with the photoAdapter
        val showErrorAdapter = photoAdapter
                .withLoadStateFooter(ErrorStateAdapter { photoAdapter.retry() })
        rcvPhoto.adapter = showErrorAdapter
        val sh = PagerSnapHelper()
        sh.attachToRecyclerView(rcvPhoto)
    }

    private fun subscribeToReceiveData() {
        lifecycleScope.launch {
            viewModel
                    .pagingFlow
                    .collectLatest { pagingData ->
                        photoAdapter.submitData(pagingData)
                    }
        }
    }

    companion object {
        fun newInstance() = PhotoBrowserFragment()
    }
}