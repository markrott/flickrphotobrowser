package com.example.flickrphotobrowser.entity

import com.example.flickrphotobrowser.entity.remote.PhotoDto
import com.example.flickrphotobrowser.entity.remote.PhotosDto
import com.example.flickrphotobrowser.entity.ui.PhotoUi
import com.example.flickrphotobrowser.entity.ui.PhotosUi

fun PhotosDto.toUi(): PhotosUi = PhotosUi(
    page = page ?: 0,
    pages = pages ?: 0,
    perPage = perPage ?: 0,
    total = total ?: 0,
    photo = photo?.map { it.toUi() } ?: emptyList()
)

fun PhotoDto.toUi(): PhotoUi = PhotoUi(
    id = id,
    owner = owner ?: "",
    url = url ?: "",
    height = height ?: 0,
    width = width ?: 0,
    title = title ?: ""
)