package com.example.flickrphotobrowser.entity.remote


import com.google.gson.annotations.SerializedName

class PhotoDto(
    @SerializedName("id")
    val id: String,
    @SerializedName("owner")
    val owner: String?,
    @SerializedName("url_z")
    val url: String?,
    @SerializedName("height_z")
    val height: Int?,
    @SerializedName("width_z")
    val width: Int?,
    @SerializedName("title")
    val title: String?
)