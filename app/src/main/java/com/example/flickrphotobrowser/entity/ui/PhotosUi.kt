package com.example.flickrphotobrowser.entity.ui

data class PhotosUi(
    val page: Int = 0,
    val pages: Int = 0,
    val perPage: Int = 0,
    val total: Int = 0,
    val photo: List<PhotoUi> = emptyList()
)