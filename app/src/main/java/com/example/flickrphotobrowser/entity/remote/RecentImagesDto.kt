package com.example.flickrphotobrowser.entity.remote

import com.google.gson.annotations.SerializedName

class RecentImagesDto(
    @SerializedName("photos")
    val photos: PhotosDto?,
    @SerializedName("stat")
    val stat: String?
)