package com.example.flickrphotobrowser.entity.ui

data class PhotoUi (
    val id: String,
    val owner: String,
    val url: String,
    val height: Int,
    val width: Int,
    val title: String
)