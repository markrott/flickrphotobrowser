package com.example.flickrphotobrowser

import android.app.Application
import com.example.flickrphotobrowser.di.dataModule
import com.example.flickrphotobrowser.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MyApp)
            modules(networkModule, dataModule)
        }
    }
}