**This application displays photos that are obtained from the flickr service.**

Link to youtube: https://youtu.be/kyYquYkDhho

---


**To `RUN` a project, you need to get the developer's keys from _Flickr_: https://www.flickr.com/services/apps/create/**


![Add secret key to project](/app/src/main/assets/f0.jpg)

---

**To develop this application, I used the following technologies:**

- Paging3
- Coroutines
- ViewModel
- Flow
- Koin
- Glide
- Retrofit
- Gson

---

**Images from application:**

![Photo item](/app/src/main/assets/f1.jpg)
![Error item and retry logic](/app/src/main/assets/f2.jpg)
